// 1.Когда не уверен в каком-то участке кода(кричический участок)
// 2.Если нужно создать собственную ошибку
// 3.Что б избежать остановки программы, и/или  создания ошибки , которая выведется пользователю и он ее исправит(введет повторно данные или ему сообщат о задержке допустим , что б он подождал и программа будет работать дальше )
function createListOfElements() {
    let wrap = document.getElementById("root");
    const books = [
        { 
          author: "Скотт Бэккер",
          name: "Тьма, что приходит прежде",
          price: 70 
        }, 
        {
         author: "Скотт Бэккер",
         name: "Воин-пророк",
        }, 
        { 
          name: "Тысячекратная мысль",
          price: 70
        }, 
        { 
          author: "Скотт Бэккер",
          name: "Нечестивый Консульт",
          price: 70
        }, 
        {
         author: "Дарья Донцова",
         name: "Детектив на диете",
         price: 40       
        },
        {
         author: "Дарья Донцова",
         name: "Дед Снегур и Морозочка",
        }
      ];
      // const fitArr = ["author", "name", "price"];
     const newArr = books.map(function(item,i,arr){
        try{
          if(!item.name || !item.author || !item.price){
            throw new SyntaxError("Нет свойства");
          }
          else{
            return `<li>${JSON.stringify(item)}</li>`;
          }
        }
        catch (err)
        {        
          if(item.price && item.author && !item.name){
             console.log(`У ${i+1}  обьекта нет свойства name`);
        }    
          if(!item.author && item.price && item.name){
             console.log(`У ${i+1}  обьекта нет свойства author`);
        }    
          if(item.author && !item.price && item.name){
             console.log(`У ${i+1}  обьекта нет свойства price`);
        }    
        
      }
  });
      wrap.innerHTML = `${newArr.join(" ")}`; 
}

createListOfElements();