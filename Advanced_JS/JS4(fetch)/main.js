//Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
// аякс - это асинхронный JS и XML. С его помощью можно обрабатывать запросы на сервер асинхронно, те в фоновом режиме.

const root = document.getElementById("root");
fetch('https://swapi.dev/api/films/')
  .then((response) => {
      console.log(response);
    return response.json();
  })
  .then((data) => {
    console.log(data);
    let arrOfFilms = data.results;
    console.log(arrOfFilms);
    arrOfFilms.forEach(({episode_id,title,opening_crawl}) => {
       const episode = document.createElement("p");
       episode.textContent = episode_id;
       const titleField = document.createElement("p");
       titleField.textContent = title;
        const intro = document.createElement("p");
        intro.textContent = opening_crawl;
        root.append(episode,titleField,intro);
    });
  })
  .catch((error) => {
    throw new Error(error.message);
  });
