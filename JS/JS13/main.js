
const btn = document.getElementById("change-btn");
const link = document.createElement('link');
btn.onclick = function(){
    if(localStorage.getItem("bgcolor")){
        localStorage.removeItem("bgcolor");
        link.rel = 'stylesheet';
        link.href = 'style.css';
        document.head.append(link);
    }
    else{
        localStorage.setItem("bgcolor","theme");
        link.rel = 'stylesheet';
        link.href = 'theme.css';
        document.head.append(link);
    }
}
document.addEventListener("DOMContentLoaded",()=>{
  
        if(localStorage.getItem("bgcolor")){
            link.rel = 'stylesheet';
            link.href = 'theme.css';
            document.head.append(link);
        }

    });

