const email = document.getElementById("email");
const text = document.getElementById("str");
const error = document.getElementById("error");
const cancel = document.getElementById("cancel");

cancel.onclick = function() {    
    text.innerHTML = "";
    cancel.innerHTML = "";
    email.value = "";

} 

email.onfocus = function (){
    text.innerHTML =  "";
    cancel.innerHTML = "";
    email.style.border = "3px solid green";
    email.style.color = "black";
};

email.onblur = function (){
   let value = email.value;
    email.style.border = "1px solid black";
    email.style.color = "green";
    
    if(isNaN(value)){
        email.style.border = "3px solid red";
        error.innerHTML ="Please enter correct price";
        text.innerHTML = "";
        cancel.innerHTML = "";
      
    }
    else if(email.value == null || email.value ==undefined || email.value.length == 0)
    {
        text.innerHTML = "";
        cancel.innerHTML = "";
    }    
    else{
        text.innerHTML = `Текущая цена: ${value}`;
        cancel.innerHTML = "X";
    }
    
    if(email.value < 0){
        email.style.border = "1px solid red";
        error.innerHTML ="Please enter correct price";
        email.style.color = "red";
        error.style.color = "red";
        text.innerHTML = "";
        cancel.innerHTML = "";
    }  
    else if(email.value > 0){
        email.style.border = "1px solid green";
        error.innerHTML = "";
    }
};
 
