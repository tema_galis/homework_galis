let counter = 0;
const img = document.querySelectorAll(".image-to-show");
const stopBtn = document.querySelector('.stop-btn');
const continueBtn = document.querySelector('.continue-btn');
 function changeImg() {
     img[counter].classList.remove("show");
    counter = (counter + 1) % img.length;
    img[counter].classList.add("show");
 }
 let timer = setInterval(changeImg,1000);

 stopBtn.addEventListener('click', function () {
     clearInterval(timer);
     continueBtn.removeAttribute('hidden');
 });

 continueBtn.addEventListener('click', function (){
     timer = setInterval(changeImg, 1000);
     this.setAttribute('hidden', true);
 })
