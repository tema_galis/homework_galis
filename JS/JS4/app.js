
function CreateNewUser(){
    firstName = prompt  ("Write your Name");
    lastName = prompt("Write your LastName");

    newUser ={  
        getLogin(){
            let sliceName = firstName.toString();
            let newStr =  sliceName.slice(0,1); 
            return newStr.toLowerCase() + lastName.toLowerCase();       
        },    
    };
      
    Object.defineProperty(newUser,"firstName",{
        value: firstName,
        writable:true,
        enumerable:true,
        configurable:true
    });

    Object.defineProperty(newUser,"lastName",{
        value: lastName,
        writable:true,
        enumerable:true,
        configurable:true
    });
    
    console.log(newUser);
    console.log(newUser.getLogin());
    return;
}

CreateNewUser();

//2-й вариант
/*
function CreateNewUser(){
    const user = {
    getName: function () {
        let firstName = prompt("Your name");
        return firstName;
    },
    getLastName: function(){
        let lastName = prompt("Last Name");
        return lastName;
    },
    getLogin: function(){
        return this.getName().slice(0,1) + this.getLastName();
    },


}
console.log(user.getLogin());
}
CreateNewUser();
*/