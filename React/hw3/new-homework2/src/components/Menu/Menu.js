import React from 'react';
import "./Menu.scss";
import {Link} from "react-router-dom";

function Menu() {
    return (
       <nav className= "nav">
           <ul className= "nav__menu menu">
               <li className= "menu__link"><Link to = "/" className = "link">Home</Link></li>
               <li className= "menu__link"><Link to = "/cart" className = "link">InCart</Link></li>
               <li className= "menu__link"><Link to = "/favourites" className = "link">InFavourites</Link></li>

           </ul>
       </nav>
    );
}

Menu.propTypes = {};

export default Menu;