import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Products from "../Products/Products";
import InCart from "../Menu/InCart";
import InFav from "../Menu/InFav";


const Routes = (props) => {
    const {
        isLoading,
        modalOpen,
        toggleCart,
        toggleModal,
        currentProduct,
        items, toggleFavoriteCard, setCurrentProduct
    } = props;
    return (
        <Switch>
            <Route exact path="/"><Products isLoading={isLoading} modalOpen={modalOpen} toggleCart={toggleCart}
                                            toggleModal={toggleModal} currentProduct={currentProduct} items={items}
                                            toggleFavoriteCard={toggleFavoriteCard}
                                            setCurrentProduct={setCurrentProduct}/></Route>
            <Route exact path="/cart"><Products isLoading={isLoading} modalOpen={modalOpen} toggleCart={toggleCart}
                                                toggleModal={toggleModal} currentProduct={currentProduct}
                                                items={items.filter(item =>item.isInCart)}
                                                toggleFavoriteCard={toggleFavoriteCard}
                                                setCurrentProduct={setCurrentProduct}/></Route>
            <Route exact path="/favourites"><Products isLoading={isLoading} modalOpen={modalOpen}
                                                      toggleCart={toggleCart}
                                                      toggleModal={toggleModal} currentProduct={currentProduct}
                                                      items={items.filter(item =>item.isFavourite)}
                                                      toggleFavoriteCard={toggleFavoriteCard}
                                                      setCurrentProduct={setCurrentProduct}/></Route>
        </Switch>
    );
};

export default Routes;