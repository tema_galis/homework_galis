import React, {useEffect, useState} from 'react';
import ItemList from "../ItemList/ItemList";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import Loader from "../Loader/Loader";
import "./Products.scss";

function Products(props) {
    const {
        isLoading,
        modalOpen,
        toggleCart,
        toggleModal,
        currentProduct,
        items,
        toggleFavoriteCard,
        setCurrentProduct
    } = props;

    return (
        <div className="product-container">
            {isLoading ? <Loader/> :
                <>
                    {modalOpen && <ConfirmModal
                        onSubmit={() => {
                            toggleCart(currentProduct);
                            toggleModal();
                        }} onCancel={toggleModal}
                        currentProduct={currentProduct}/>}
                    <ItemList products={items} setModal={item => {
                        toggleModal();
                        setCurrentProduct(item);
                    }} toggleFavorite={toggleFavoriteCard}/>
                </>}
        </div>
    )
}

export default Products;

