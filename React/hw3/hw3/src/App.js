import React, {useEffect, useState} from 'react'
import Routes from "./components/Routes/Routes";
import Menu from "./components/Menu/Menu";


export default function App() {
    const [items, setItems] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [currentProduct, setCurrentProduct] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    const getItemsRequest = async () => {
        const response = await fetch("./db.json");
        const data = await response.json();
        const cart = localStorageCheck('cart');
        const favourites = localStorageCheck('favourites');
        const updateProducts = checkProduct(data, cart, favourites);
        setItems(updateProducts);
    }

    const localStorageCheck = (field) => {
        const checkItem = localStorage.getItem(field);
        if (!checkItem) {
            localStorage.setItem(field, JSON.stringify([]));
            return [];
        }
        return JSON.parse(checkItem);
    }

    useEffect(() => {
        getItemsRequest().then(() => setIsLoading(false));
    }, []);

    const checkProduct = (data, cart, favourites) => {
        return data.map((items) => {
            const productInCart = cart.some(id => items.id === id);
            const favouritesInCart = favourites.some(id => items.id === id);
            return {
                ...items,
                isInCart: productInCart,
                isFavourite: favouritesInCart
            }
        });
    }

    const toggleFavoriteCard = (item) => {

        item.isFavourite = !item.isFavourite;
        const newItems = items.map(elem => elem.id === item.id ? item : elem);
        const favorites = items.filter(elem => elem.isFavourite).map(item => item.id);

        console.log(favorites);
        localStorage.setItem("favourites", JSON.stringify(favorites));
        setItems(newItems);
    }


    const toggleCart = (item) => {
        item.isInCart = !item.isInCart;
        const newItems = items.map(elem => elem.id === item.id ? item : elem);
        const inCart = items.filter(elem => elem.isInCart).map(item => item.id);
        localStorage.setItem("cart", JSON.stringify(inCart));
        setItems(newItems);
        setCurrentProduct({});
    }



    const toggleModal = () => {
        setModalOpen(!modalOpen);
    }

    return (
        <>
            <Menu/>
            <Routes isLoading={isLoading} modalOpen={modalOpen} toggleCart={toggleCart}
                    toggleModal={toggleModal} currentProduct={currentProduct} items={items}
                    toggleFavoriteCard={toggleFavoriteCard}
                    setCurrentProduct={setCurrentProduct}/>
        </>

    )
}
