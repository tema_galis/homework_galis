import React from 'react';
import { connect } from "react-redux";

function Test(props) {
    const {name,age,dispatch} = props;
    const incrementAge = ()=> {
        dispatch({type:"INCREMENT_AGE"});
    }

    return (
        <div>
            <h2>Test</h2>
            <div>{name}</div>
            <div>{age}</div>
            <div><button onClick={incrementAge}>increment Age</button></div>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        name: state.name,
        age: state.age
    }
}
export default connect(mapStateToProps)(Test);
