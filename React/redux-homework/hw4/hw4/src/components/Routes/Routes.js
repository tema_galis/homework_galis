import React from 'react';
import { Switch, Route} from "react-router-dom";
import Products from "../Products/Products";
import {useSelector} from "react-redux";
import {getItems} from "../../store/products/selectors";


const Routes = () => {
    const items = useSelector(getItems);
    return (
        <Switch>
            <Route exact path="/"><Products
                items={items}
            /></Route>
            <Route exact path="/cart"><Products
                items={items.filter(item => item.isInCart)}
            /></Route>
            <Route exact path="/favourites"><Products
                items={items.filter(item => item.isFavourite)}
            /></Route>
        </Switch>
    );
};

export default Routes;