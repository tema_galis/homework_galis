import React from 'react'
import Card from "../Card/Card";
import "./ItemList.scss";
import PropTypes from 'prop-types';

function ItemList(props) {
    const {products, setModal, toggleFavorite} = props;
    return (
        <div className="item-list">
            {products.map(item => {
                return <Card key={item.id} product={item}
                             setModal={() => setModal(item)}
                             toggleFavorite={() => toggleFavorite(item)}/>
            })}
        </div>
    )
}

export default ItemList


ItemList.propTypes = {
    products: PropTypes.array.isRequired,
    setModal: PropTypes.func.isRequired,
    toggleFavorite: PropTypes.func.isRequired
}


