import React from 'react';
import ItemList from "../ItemList/ItemList";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import Loader from "../Loader/Loader";
import "./Products.scss";
import {useDispatch, useSelector} from "react-redux";
import {
    setCurrentProductAction,
    setItemsAction,
    toggleModalAction
} from "../../store/products/actions";
import {getCurrentProduct, getIsLoading, getIsModalOpen} from "../../store/products/selectors";

function Products(props) {
    const {
        items,
    } = props;


    const dispatch = useDispatch();
    const currentProduct = useSelector(getCurrentProduct);
    const isLoading = useSelector(getIsLoading)
    const modalOpen = useSelector(getIsModalOpen);

    const toggleModal = () => {
        dispatch(toggleModalAction());
    }

    const toggleCart = (item) => {
        item.isInCart = !item.isInCart;
        const newItems = items.map(elem => elem.id === item.id ? item : elem);
        const inCart = items.filter(elem => elem.isInCart).map(item => item.id);
        localStorage.setItem("cart", JSON.stringify(inCart));
        dispatch(setItemsAction(newItems));
        dispatch(setCurrentProductAction({}))
    }

    const toggleFavoriteCard = (item) => {
        item.isFavourite = !item.isFavourite;
        const newItems = items.map(elem => elem.id === item.id ? item : elem);
        const favorites = items.filter(elem => elem.isFavourite).map(item => item.id);
        localStorage.setItem("favourites", JSON.stringify(favorites));
        dispatch(setItemsAction(newItems));
    }

    return (
        <div className="product-container">
            {isLoading ? <Loader/> :
                <>
                    {modalOpen && <ConfirmModal
                        onSubmit={() => {
                            toggleCart(currentProduct, dispatch);
                            dispatch(toggleModalAction());
                        }} onCancel={toggleModal}
                        currentProduct={currentProduct}/>}
                    <ItemList products={items} setModal={item => {
                        dispatch(toggleModalAction());
                        dispatch(setCurrentProductAction(item));
                    }} toggleFavorite={toggleFavoriteCard}/>
                </>}
        </div>
    )
}

export default Products;

