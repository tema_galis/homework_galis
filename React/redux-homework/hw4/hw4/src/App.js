import './App.css';
import React, {useEffect} from 'react'

import {useDispatch} from "react-redux";
import Routes from "./components/Routes/Routes";
import Menu from "./components/Menu/Menu";
import {setIsLoadingAction, setItemsAction} from "./store/products/actions";


function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getItemsRequest());
    }, []);

    const getItemsRequest = () => {
        return async function (dispatch){
            dispatch(setIsLoadingAction(true))
            const response = await fetch("./db.json");
            const data = await response.json();
            const cart = localStorageCheck('cart');
            const favourites = localStorageCheck('favourites');
            const updateProducts = checkProduct(data, cart, favourites);
            dispatch(setItemsAction(updateProducts));
            dispatch(setIsLoadingAction(false))
        }
    }


    const localStorageCheck = (field) => {
        const checkItem = localStorage.getItem(field);
        if (!checkItem) {
            localStorage.setItem(field, JSON.stringify([]));
            return [];
        }
        return JSON.parse(checkItem);
    }

    const checkProduct = (data, cart, favourites) => {
        return data.map((items) => {
            const productInCart = cart.some(id => items.id === id);
            const favouritesInCart = favourites.some(id => items.id === id);
            return {
                ...items,
                isInCart: productInCart,
                isFavourite: favouritesInCart
            }
        });
    }

    return (
        <div className="App">
            <Menu/>
            <Routes/>
        </div>
    );
}

export default App;
