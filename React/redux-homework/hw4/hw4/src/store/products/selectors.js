export const getIsLoading = ({isLoading}) => isLoading;
export const getCurrentProduct = ({currentProduct}) => currentProduct;
export const getIsModalOpen = ({modalOpen}) => modalOpen;
export const getItems = ({items}) => items;