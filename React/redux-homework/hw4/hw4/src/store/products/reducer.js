import React from 'react'
import * as types from "./types";
const initialState = {
    items: [],
    modalOpen:false,
    currentProduct:{},
    isLoading:false,
}

export default function RootReducer(state = initialState,action){
    switch(action.type){
        case types.TOGGLE_MODAL:{
            return {...state,modalOpen: !state.modalOpen}
        }
        case types.SET_IS_LOADING:{
            return {...state, isLoading: action.payload}
        }
        case types.SET_CURRENT_PRODUCT:{
            return {...state, currentProduct: action.payload}
        }
        case types.SET_ITEMS:{
            return {...state, items: action.payload}
        }
        default:return state;
    }
}


