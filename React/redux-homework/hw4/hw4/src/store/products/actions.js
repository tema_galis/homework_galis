import * as types from "./types";

export const toggleModalAction = ()=>({
    type:types.TOGGLE_MODAL
})

export const setIsLoadingAction = isLoading => ({
    type: types.SET_IS_LOADING,
    payload: isLoading
})

export const setItemsAction = items => ({
    type: types.SET_ITEMS,
    payload: items
})

export const setCurrentProductAction = currentProduct => ({
    type: types.SET_CURRENT_PRODUCT,
    payload: currentProduct
})
