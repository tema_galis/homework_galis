import React from 'react'
import * as types from "./types";
const initialState = {
    items: [],
    modalOpen:false,
    currentProduct:{},
    isLoading:true,
}

export default function RootReducer(state = initialState,action){
    switch(action.type){
        case types.TOGGLE_MODAL:{
            return {...state,modalOpen: !state.modalOpen}
        }

        default:return state;
    }
}


