import React from 'react';
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import "./ConfirmModal.scss";
import PropTypes from 'prop-types';

function ConfirmModal(props) {
    const {onCancel, onSubmit, currentProduct} = props;
    const {isInCart} = currentProduct;

    return (
        <div className="modal">
            <div className="modalBackDrop" onClick={onCancel}/>
            <div className="modalDialog">
                <div className="modalContent">
                    <div className="modalHeader">
                        <h3 className="modalTitle">{isInCart ? 'Delete from cart' : 'Add to cart'}</h3>
                        <ConfirmButton text="x" className="close" onClick={onCancel}/>
                    </div>
                    <div className="modal-body">
                        <div className="ModalBody">
                            {isInCart ? 'Delete from cart ?' : `Add to cart ?`}
                        </div>
                    </div>
                    <div className="ModalFooter">
                        <ConfirmButton onClick={onCancel} className="btn secondaryButton" text='Cancel'/>
                        <ConfirmButton className='btn primaryButton' text={isInCart ? 'Delete' : 'Add'}
                                       onClick={onSubmit}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ConfirmModal


ConfirmModal.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}

ConfirmModal.defaultProps = {
    isInCart: false,
    currentProduct: {}
}
