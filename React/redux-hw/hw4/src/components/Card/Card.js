import React from 'react'
import "./Card.scss";
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import Star from "../Star/Star";
import StarYellow from "../Star/StarYellow";
import DeleteButton from "../DeleteButton/DeleteButton";
import PropTypes from 'prop-types';

function Card(props) {
    const {product, setModal, toggleFavorite} = props;
    const {name, price, url, colour, isFavourite, isInCart} = product;

    return (

        <div className="wrapper " style={{backgroundColor: colour}}>
            {isInCart ? (
                <>
                    <DeleteButton action = {setModal}/>
                    <span>{name}</span>
                    <img src={url} alt={"car"} className="card-img"/>
                    {isFavourite ? (
                        <StarYellow action={toggleFavorite}/>
                    ) : (
                        <Star action={toggleFavorite}/>
                    )}
                    <span>{price}</span>
                    <ConfirmButton text={isInCart ? "In cart" : "Add to Cart"} bgc={isInCart ? "red " : "green"}
                                   onClick={setModal}/>
                </>
            ) : (
                <>

                    <span>{name}</span>
                    <img src={url} alt={"car"} className="card-img"/>
                    {isFavourite ? (
                        <StarYellow action={toggleFavorite}/>
                    ) : (
                        <Star action={toggleFavorite}/>
                    )}
                    <span>{price}</span>
                    <ConfirmButton text={isInCart ? "In cart" : "Add to Cart"} bgc={isInCart ? "red "  : "green"}
                                   onClick={setModal}/>
                </>
            )}


        </div>
    )
}

export default Card

Card.propTypes = {
    product: PropTypes.object.isRequired,
    setModal: PropTypes.func.isRequired,
    toggleFavorite: PropTypes.func.isRequired
}
Card.defaultProps = {
    product: {
        colour: "red",
        name: "mercedes s63",
        url: "https://wroom.ru/i/cars2/mercedesbenz_visioneqs_1.jpg",
        price: "124565",
        isInFavourite: false
    }
}


