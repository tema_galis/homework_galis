import React, { PureComponent } from 'react'
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import "./ConfirmModal.scss";
export default class ConfirmModal extends PureComponent {

   
    componentDidMount(){
        document.addEventListener("click",this.handleOutsideClick);
      }
      componentWillUnmount(){
        document.removeEventListener("click",this.handleOutsideClick);
      }
      handleOutsideClick=(e)=>{
        const {hideModal} = this.props;
        if(e.target.className === "modal"){
          hideModal();
        }
      }

    render() {
     
        const {click,isOpen,children,onSubmit,onCancel,title,id,} = this.props;
        // const {text,actions,header,closeBtn} = info;
        return (
          <>
              {isOpen &&
            <div className = "modal" >
            <section className="modal-main">   
              {/* {closeBtn && <ConfirmButton click = {click} text = {"X"}/>}  */}
              {/* <h1 className = "header">{header}</h1>
              <div className = "text" >{text}</div>
              <div className = "btns">{actions}</div> */}
              <ConfirmButton onClick = {onCancel}/>
              <ConfirmButton onClick = {onSubmit}/>
            </section>
          </div>
          }
          </>
          
        )
    }
}
