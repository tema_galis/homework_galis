import React, { Component, Fragment } from 'react'



export default class Tab extends Component{

render(){
    const {text,active,onSelect} = this.props;
    return(
        <div onClick = {onSelect} style = {{borderBottom: active? "2px solid black" : ""}}>
           {text}
        </div>
    )
}

}

    
