import React, { Component, Fragment } from 'react'
import "./Card.scss";
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
export default class Card extends Component{
    // state = {
    //     isOpen:false,
    //     modal:"",
    // }
    
//  toggleModal=(modalType)=>{
//     this.setState({isOpen:!this.state.isOpen,modal:modalType});
  
//   }
  // hideModal=()=>{
  //   this.setState({isOpen:false});
  // }
  
  // modalInfo = {
  //   first:{
  //     closeBtn:true,
  //        header:"Add to cart?",
  //        actions:<><ConfirmButton text = {"Ok"} click = {this.toggleModal} style = {{backgroundColor :this.props.bgc} }/> <ConfirmButton text = {"Close"} click = {this.toggleModal} /></>
  //   }
  // }
render(){

const {name, price, url, articul, colour, onClick,handleModalOpen,id} = this.props;
// const {isOpen} = this.state;
    return(
        <div className = "wrapper " style = {{backgroundColor: colour}}>
        <span>{name}</span>
        <img src = {url} className = "card-img"/>
        <span >{price}</span>
        <ConfirmButton  text = {"Add to Cart"} bgc = {"green"} handleModalOpen = {()=> handleModalOpen(id)}  />
       
        {/* <ConfirmModal info = {this.modalInfo[this.state.modal]} click = {this.toggleModal} hideModal={this.hideModal} /> */}
      
        </div>
    )
}

}

    
