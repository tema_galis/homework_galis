import React, { PureComponent } from 'react'

export default class ConfirmButton extends PureComponent {
    render() {
        return (
            <div>
                  <button className = "btn-component"  style = {{backgroundColor :this.props.bgc} } onClick = {this.props.onClick}>{this.props.text}</button>
            </div>
        )
    }
}
