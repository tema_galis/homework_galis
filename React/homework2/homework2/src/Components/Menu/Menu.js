import React, { Component, Fragment } from 'react'
import Tab from "../Tab/Tab"; 


export default class Menu extends Component{
 
render(){
    const {activeItem,onSelectMenuItem} = this.props;
    return(
        <div style = {{display:"flex"}}>
        
        <Tab onSelect = {()=>
            {onSelectMenuItem("Cart")}
            } text = {"Cart"} active = {activeItem === "Cart"}/>

        <Tab onSelect = {()=>
            {onSelectMenuItem("Cards")}
            } text = {"Cards"} active = {activeItem === "Cards"}/>
        </div>
    )
}

}

    
