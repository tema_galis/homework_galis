import React, { PureComponent } from 'react';
import ItemList from "../ItemList/ItemList";
import ConfirmModal from "../ConfirmModal/ConfirmModal";

export default class Products extends PureComponent {
    state = {
        isLoading:false,
        error:null,
        modalOpen:false,
        id:null,
        items:[],
    }
    getItems(){
        const response =  fetch(`./goods.json`);
        const data =  response.json();
        console.log(data);
        const cart = JSON.parse(localStorage.getItem("cart"));
        const favourites = JSON.parse(localStorage.getItem("favourites"));
        const updateProducts = this.checkProduct(data,cart,favourites);
        console.log(updateProducts);
        this.setState({
                  ...this.state,
                  isLoading: false,
                  items:updateProducts
                })
    }
    componentDidMount(){
        
        if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
            localStorage.setItem('cart', JSON.stringify([]))
            localStorage.setItem('favorites', JSON.stringify([]))
        }
        // const response = await fetch("./goods.json");
        // const data = await response.json();
        // console.log(data);
        // const cart = JSON.parse(localStorage.getItem("cart"));
        // const favourites = JSON.parse(localStorage.getItem("favourites"));
        // const updateProducts = this.checkProduct(data,cart,favourites);
        // console.log(updateProducts);
        // this.setState({
        //           ...this.state,
        //           isLoading: false,
        //           items:updateProducts
        //         })
         this.getList();
      }
      checkProduct(data,cart,favourites){
        console.log(data);
        console.log(cart);
        console.log(favourites);
        const newData = data.map((item)=>{
            const productInCart = cart.some(({id}) =>( item.id === id));
            const favouritesInCart = favourites.some(({id}) =>(item.id === id));
            return {
                ...item,
                isInCart:productInCart,
                isInFavourites:favouritesInCart
            }
        })
        return newData;
      }
      addProductInCart(id){
        const cart = JSON.parse(localStorage.getItem("cart"));
        const item = this.state.items.find((item) => item.id === id);
        cart.push(item);
        localStorage.setItem("cart", JSON.stringify([]));
        this.setState((state)=>({
            ...state,
            modalOpen:!state.modalOpen,
            id:null
        })
            
        )
      
      }
      isModalOpen(id){
        if(id){
            this.setState((state)=>({
                ...state,
                modalOpen:!state.modalOpen,
                id:id
            })
            )
        }
        else{
            this.setState((state)=>({
                ...state,
                modalOpen:state.modalOpen,
                id:null
            })
            )
        }
      }
      
    render() {
        return (
            <div>
                 <ConfirmModal isOpen={this.state.isModalOpen} children={"Добавить товар в корзину?"}
                       onSubmit={(id) => this.addProductToCart(id)} onCancel={() => this.isModalOpen()} title='Добавление в корзину' id={this.state.id}/>
                <ItemList products={this.state.items} handleModalOpen={this.isModalOpen.bind(this)} onClick={(id) => this.addToFavorites(id)}/>
            </div>
        )
    }
}
