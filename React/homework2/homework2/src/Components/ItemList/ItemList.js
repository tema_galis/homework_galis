import React, { Component, Fragment } from 'react'
import Card from "../Card/Card";
import "./ItemList.scss";
import Products from "../Products/Products";

 export default class ItemList extends Component{
    
render(){
  const {products, handleModalOpen, onClick} = this.props;

    return(
        <div className = "item-list">
         {products.map((item)=>{  
                    
             return <Card key = {item.articul} name = {item.name} price = {item.price} url = {item.url} colour = {item.colour} onClick = {onClick} handleModalOpen = {handleModalOpen} />
         })}
        </div>
    )
}

}


    
