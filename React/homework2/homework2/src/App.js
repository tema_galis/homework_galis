import './App.css';
import React, { Component, Fragment } from 'react';
import Menu from "./Components/Menu/Menu";
import ItemList from "./Components/ItemList/ItemList";

import Products from "./Components/Products/Products";


export default class App extends Component{
state = {
  data:[],
  isLoading:false,  
  cartUsers:[],
  activeSection:"Cards",
}

  setActiveSection = (section)=>{
    this.setState({activeSection:this.state.activeSection = section})
    
 }
//   renderSection = ()=>{
//     const {data,isLoading,cartUsers} = this.state;
//    if(this.state.activeSection === "Cards"){
//      return <ItemList/>
//    }
//    if(this.state.activeSection === "Cart"){
//      return <CartList/>
//    }
    
//  }

 

render(){

  return(
   
    <div className = "container">
      <Menu activeItem = {this.state.activeSection} onSelectMenuItem = {(section)=>{
            this.setActiveSection(section);
        }}/>
      {/* <div>{this.renderSection()}</div> */}
      <Products/>
    </div>
  )
}

}