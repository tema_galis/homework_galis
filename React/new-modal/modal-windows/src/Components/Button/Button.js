import React, { Component } from 'react'
import "./Button.css";
class Button extends Component {
    
    render() {
        return (
            <div>
                <button className = "btn-component"  style = {{backgroundColor :this.props.bgc} } onClick = {e => this.props.click && this.props.click()}>{this.props.text}</button>
               
            </div>
        )
    }
}
export default Button;
