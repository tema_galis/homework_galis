import React, { Component } from 'react'
import "./Modal.css";
import Button from "../Button/Button";
class Modal extends Component {
 
  componentDidMount(){
    document.addEventListener("click",this.handleOutsideClick);
  }
  componentWillUnmount(){
    document.removeEventListener("click",this.handleOutsideClick);
  }
  handleOutsideClick=(e)=>{
    const {hideModal} = this.props;
    if(e.target.className === "modal"){
      hideModal();
    }
  }
    render() {  
      const {info,click} = this.props;
      const {text,actions,header,closeBtn} = info;
        return (
            <div className = "modal" >
            <section className="modal-main">   
           
              {closeBtn && <Button click = {click} text = {"X"}/>}
              <h1 className = "header">{header}</h1>
              <div className = "text">{text}</div>
              <div className = "btns">{actions}</div>
            </section>
          </div>
        )
    }
}
export default Modal;
