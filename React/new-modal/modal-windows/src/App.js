import React, { Component, Fragment } from 'react'
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';

class App extends Component {
  state = {
    isOpen: false,
    modal:"",

  }
   toggleModal=(modalType)=>{
    this.setState({isOpen:!this.state.isOpen,modal:modalType});
 
  }
  hideModal=()=>{
    this.setState({isOpen:false});
  }
 
  modalInfo = {
    first:{
      closeBtn:true,
         header:"Do You want to Save this file?",
         text: "Once you save this file ,it will be saved forever",
         actions:<><Button text = {"Ok"} click = {this.toggleModal} style = {{backgroundColor :this.props.bgc} }/> <Button text = {"Close"} click = {this.toggleModal} /></>
    },
    second:{
      header:"Do You want to delete this file?",
        closeBtn:false,
        text: "Once you delete this file ,it will be deleted forever",
        actions:<><Button text = {"Ok"} click = {this.toggleModal}/><Button text = {"Close"} click = {this.toggleModal}/></>
    }
  }

  render() {
    const {isOpen} = this.state;
    return (
      <div>
   <Button text = {"Открыть 1"} bgc = {"green"} click = {e =>{this.toggleModal("first")}}  />
   <Button text = {"Открыть 2"} bgc = {"red"} click = {e =>{this.toggleModal("second")}}  />
   {isOpen && <Modal info = {this.modalInfo[this.state.modal]} click = {this.toggleModal} hideModal={this.hideModal} />}
   </div>
    )
  }
}
export default App;