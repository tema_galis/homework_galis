import React from 'react'
import Card from "../Card/Card";
import "./ItemList.scss";
import PropTypes from 'prop-types';

//  export default class ItemList extends Component{

// render(){

//   const {products, isModalOpen,addFavouriteCard} = this.props;

//     return(
//         <div className = "item-list">
//          {products.map((item)=>{  
//              return <Card key = {item.id} id  = {item.id} name = {item.name} price = {item.price} url = {item.url} colour = {item.colour}  isModalOpen = {isModalOpen} addFavouriteCard = {()=>addFavouriteCard(item)} isInFavourite = {item.isFavourite}/>
//          })}
//         </div>
//     )
// }
// }
// import React from 'react'

function ItemList(props) {
    const {products, isModalOpen,addFavouriteCard} = props;
    return (
        <div className = "item-list">
         {products.map((item)=>{  
             return <Card key = {item.id} id  = {item.id} name = {item.name} price = {item.price} url = {item.url} colour = {item.colour}  isModalOpen = {isModalOpen} addFavouriteCard = {()=>addFavouriteCard(item)} isInFavourite = {item.isFavourite}/>
         })}
        </div>
    )
}

export default ItemList


ItemList.propTypes = {
    products: PropTypes.array.isRequired,
    isModalOpen: PropTypes.func.isRequired,
    addFavouriteCard:PropTypes.func.isRequired
}


