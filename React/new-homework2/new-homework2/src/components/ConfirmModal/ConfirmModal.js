import React from 'react';
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import "./ConfirmModal.scss";
import PropTypes from 'prop-types';
// export default class ConfirmModal extends React.Component {


//     render() {
//         const {isOpen,onCancel,title,} = this.props;
    
//         return (
//             <>
//                 {isOpen &&
//                 <div className="modal">
//                     <div className="modalBackDrop" onClick={onCancel}/>
//                     <div className="modalDialog">
//                         <div className="modalContent">
//                             <div className="modalHeader">
//                                 <h3 className="modalTitle">{title}</h3>
//                                 <ConfirmButton text="x" className="close" onClick={this.props.onCancel}/>
//                             </div>
//                             <div className="modal-body">
//                                 <div className="ModalBody">
//                                     {this.props.children}
//                                 </div>
//                             </div>
//                             <div className="ModalFooter">
//                                 <ConfirmButton onClick={this.props.onCancel} className="btn secondaryButton" text='Cancel'/>
//                                 <ConfirmButton className='btn primaryButton' text='Submit' onClick={() => {this.props.onSubmit(this.props.id)}}/>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 }
//              </>
          
//         )
//     }
// }

function ConfirmModal(props) {
    const {isOpen,onCancel,title,children,id,onSubmit} = props;
    return (
        <>
        {isOpen &&
        <div className="modal">
            <div className="modalBackDrop" onClick={onCancel}/>
            <div className="modalDialog">
                <div className="modalContent">
                    <div className="modalHeader">
                        <h3 className="modalTitle">{title}</h3>
                        <ConfirmButton text="x" className="close" onClick={onCancel}/>
                    </div>
                    <div className="modal-body">
                        <div className="ModalBody">
                            {children}
                        </div>
                    </div>
                    <div className="ModalFooter">
                        <ConfirmButton onClick={onCancel} className="btn secondaryButton" text='Cancel'/>
                        <ConfirmButton className='btn primaryButton' text='Submit' onClick={() => {onSubmit(id)}}/>
                    </div>
                </div>
            </div>
        </div>
        }
     </>
    )
}

export default ConfirmModal


ConfirmModal.propTypes = {
    isOpen:PropTypes.bool,
    onCancel:PropTypes.func.isRequired,
    title: PropTypes.string

}


ConfirmModal.defaultProps = {
    title: "New title"
}