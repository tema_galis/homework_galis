import React, {PureComponent,useState,useEffect} from 'react';
import ItemList from "../ItemList/ItemList";
import ConfirmModal from "../ConfirmModal/ConfirmModal";


function Products() {
    const [items,setItems] = useState([]);
    const [isLoading,setIsLoading] = useState(false);
    const [error,setError] = useState(null);
    const [modalOpen,setModalOpen] = useState(false);
    const [currentId,setCurrentId] = useState(null);
    const [isInFavourite,setIsInFavourites] = useState(null);

    const getItemsRequest = async () => {
        if (!localStorage.getItem('cart') || !localStorage.getItem('favourites')) {
            localStorage.setItem('cart', JSON.stringify([]))
            localStorage.setItem('favourites', JSON.stringify([]))
        }
        const response = await fetch("./db.json");
        const data = await response.json();
        const cart = JSON.parse(localStorage.getItem("cart"));
        const favourites = JSON.parse(localStorage.getItem("favourites"));
        const updateProducts = checkProduct(data, cart, favourites);
        setIsLoading(true);
        setItems(updateProducts);
    }
    useEffect(()=>{
        getItemsRequest();
    },[]);
    const checkProduct = (data, cart, favourites)=>{
        return data.map((items) => {
            const productInCart = cart.some(({id}) => (items.id === id));
            const favouritesInCart = favourites.some(({id}) => (items.id === id));
            return {
                ...items,
                isInCart: productInCart,
                isFavourite: favouritesInCart
            }
        });
    }
    const addProductInCart = (id)=>{
        const cart = JSON.parse(localStorage.getItem('cart'));
        const product = items.find((product) => product.id === id)
        cart.push(product);
        localStorage.setItem('cart', JSON.stringify(cart))
        setModalOpen(!modalOpen);
        setCurrentId(null);
    }
    const addFavouriteCard = (item) => {
        console.log(item);
        item.isFavourite = !item.isFavourite;
        const items = items.map(elem => elem.id === item.id ? item : elem);
        const favorites = items.filter(elem => elem.isFavourite);
        localStorage.setItem("favourites", JSON.stringify(favorites));
        setItems(items);

    }
    const isModalOpen=(id)=> {
        if (id) {
            setModalOpen(!modalOpen);
            setCurrentId(id)
        } else {
            setModalOpen(modalOpen);
            setCurrentId(null)
        }
    }
    return (
        <div>
            <ConfirmModal isOpen={modalOpen} children={"Добавить товар в корзину?"}
                              onSubmit={(id) => addProductInCart(id)} onCancel={() => isModalOpen()} title='Добавление в корзину' id={currentId}/>
            <ItemList products={items} isModalOpen={isModalOpen.bind(this)} addFavouriteCard={addFavouriteCard}/>
        </div>
    )
}

export default Products

