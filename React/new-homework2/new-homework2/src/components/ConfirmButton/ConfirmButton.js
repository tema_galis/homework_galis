import React  from 'react'
import "./ConfirmButton.scss";
import PropTypes, { string } from 'prop-types';
// export default class ConfirmButton extends PureComponent {
//     render() {
//         const {bgc,onClick,text} = this.props;
//         return (
//             <div className = "btn">
//                   <button className = "btn__component"  style = {{backgroundColor :bgc} } onClick = {onClick}>{text}</button>
//             </div>
//         )
//     }
// }
// import React from 'react'

function ConfirmButton(props) {
    const {bgc,onClick,text} = props;
    return (
        <div className = "btn">
            <button className = "btn__component"  style = {{backgroundColor :bgc} } onClick = {onClick}>{text}</button>
        </div>
    )
}

export default ConfirmButton


ConfirmButton.propTypes = {
    bgc:PropTypes.string,
    onClick:PropTypes.func.isRequired,
    text:string
}
ConfirmButton.defaultProps = {
    bgc:"white",
    text:"Click"
}
