import React from 'react'
import "./Card.scss";
import ConfirmButton from "../ConfirmButton/ConfirmButton";
import Star from "../Star/Star";
import StarYellow from "../Star/StarYellow";
import PropTypes from 'prop-types';


// export default class Card extends Component{
// render(){

// const {name, price, url, colour,isModalOpen,id,addFavouriteCard,isInFavourite } = this.props;
//     return(
//         <div className = "wrapper " style = {{backgroundColor: colour}}>
//             <span>{name}</span>
//             <img src = {url} className = "card-img"/>
//             {isInFavourite ? (
//             <StarYellow toggleIcon = {addFavouriteCard}/>
//             ) :(
//             <Star toggleIcon = {addFavouriteCard}/>
//             )}  
          
//             <span >{price}</span>
//             <ConfirmButton  text = {"Add to Cart"} bgc = {"green"} onClick = {() => isModalOpen(id)}/>
//         </div>

//     )
// }}
// import React from 'react'

function Card(props) {
    const {name, price, url, colour,isModalOpen,id,addFavouriteCard,isInFavourite } = props;
    return (
        <div className = "wrapper " style = {{backgroundColor: colour}}>
        <span>{name}</span>
        <img src = {url} className = "card-img"/>
        {isInFavourite ? (
        <StarYellow toggleIcon = {addFavouriteCard}/>
        ) :(
        <Star toggleIcon = {addFavouriteCard}/>
        )}  
      
        <span >{price}</span>
        <ConfirmButton  text = {"Add to Cart"} bgc = {"green"} onClick = {() => isModalOpen(id)}/>
    </div>
    )
}

export default Card

Card.propTypes = {
    colour : PropTypes.string,
    name:PropTypes.string,
    url:PropTypes.string,
    price:PropTypes.string,
    isModalOpen:PropTypes.func.isRequired,
    id:PropTypes.string,
    addFavouriteCard:PropTypes.func.isRequired,
    isInFavourite:PropTypes.bool
}
Card.defaultProps = {
    colour: "red",
    name: "mercedes s63",
    url: "https://wroom.ru/i/cars2/mercedesbenz_visioneqs_1.jpg",
    price: "124565",
    id: "myid2",
    isInFavourite: false
    
}

    
